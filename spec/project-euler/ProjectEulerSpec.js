describe("Project Euler", function() {

  var Respostas = require('../../lib/project-euler/ProjectEuler');
  var respostas;

  beforeEach(function() {
    respostas = new Respostas();
  });

  afterEach(function() {
    console.log("Resposta do " + this.problema + ": " + this.resposta.toString());
  });

  it("deveria achar a soma de todos os múltiplos de 3 e 5 antes de 1000", function() {
    
    this.problema = "problema 1";
    this.resposta = respostas.problema1();
    
    expect(this.resposta).toEqual(233168);
  });

});