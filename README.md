# Project Euler #

Este repositório contém as respostas para os exercícios do site [Project Euler](https://projecteuler.net/).
Os problemas e seus testes foram codificados em Javascript.

### Como setar o ambiente? ###

* Instale o Node
* Instale o Jasmine:
    `npm install -g jasmine`
* Siga os exemplos para codificar os exercícios
* E então rode os testes:
    `jasmine`