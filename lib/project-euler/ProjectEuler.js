function ProjectEuler() { }

/**
 * Problema 1:
 *
 * Find the sum of all the multiples of 3 or 5 below 1000.
 */
ProjectEuler.prototype.problema1 = function() {
        
    var multiplesSum = 0;

    for (var i = 1; i <1000; i++) {
        if (i % 3 == 0 || i % 5 == 0) {
            multiplesSum += i;
        }
    }

    return multiplesSum;

};

module.exports = ProjectEuler;
